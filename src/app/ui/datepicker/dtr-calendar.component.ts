import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { NgbModal, NgbDateStruct, NgbDate, NgbCalendar, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { AbstractControl } from '@angular/forms';
//import {HolidayService} from '../../holidays/services/holiday.service';
import {I18n, CustomDatepickerI18n} from './i18n/datepicker-i18n';
import * as Holidays from 'date-holidays';


@Component({
  selector: 'wdsk-app-calendar',
  templateUrl: './dtr-calendar.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./dtr-calendar.component.scss'],
  providers: [I18n, { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }]
})
export class CalendarComponent implements OnInit {

  @Input("selectedDate")
  selectedDate: string;

  
  ngbPopoverRef: any;
  calCurrSelectedDate: NgbDate;
  today: number;
  minDateVal: NgbDate;
  maxDateVal: NgbDate;
  hoveredDate: NgbDate;
  fromDate: NgbDate;
  toDate: NgbDate;
  formattedDate: string;
  dateReviewAuditType: string = 'review';
  fromDateErrorMsg: string = "";
  reviewCheckVal: string = "checked";
  auditCheckVal: string = "";
  latest: string;
  @Output() selectedDateChange = new EventEmitter();
  @Output()  togglecalendar  =  new  EventEmitter();

  constructor(private modalService: NgbModal, 
    private calendar: NgbCalendar,
    
    private _i18n: I18n) { }

  ngOnInit() {
    if (!this.selectedDate) {
      this.dateTypeSelectionChanged('review');
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.showSelectedDate();
    }, 1000);

    if(document.getElementById('ngb-popover-0')){
      document.getElementById('ngb-popover-0').style.marginLeft = '2.5rem';
      document.getElementById('ngb-popover-0').style.marginTop = '2.5rem';
    }
    
  }

  /**
   * Executed whenever any input property changes.
   * @param changes 
   */
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName == "selectedDate") {
        let change = changes[propName];
        let curVal = change.currentValue;
        let prevVal = change.previousValue;

        if (curVal) {
          if (curVal.indexOf("-") > 0) {//Handles preprovided audit date
            this.dateReviewAuditType = "audit";
            this.reviewCheckVal = "";
            this.auditCheckVal = "checked";
            let inputfromDate = curVal.substring(0, 8);
            let inputToDate = curVal.substring(9);

            if (inputfromDate) {
              let auditFromNgbDate = this.getNgbDateFromStringDate(inputfromDate);
              this.checkFutureDate(auditFromNgbDate);
              this.checkFromDateValidity(auditFromNgbDate);
              this.fromDate = auditFromNgbDate;
            }

            if (inputToDate) {
              let auditToNgbDate = this.getNgbDateFromStringDate(inputToDate);
              this.checkFutureDate(auditToNgbDate);
              this.toDate = auditToNgbDate;
            }
            if (inputfromDate && inputToDate) {
              this.dateTypeSelectionChanged('audit');
              let isValid = this.daysDiffernceValid(this.fromDate, this.toDate);
              if (isValid) {
                this.formattedDate = this.getDisplayableDate(this.fromDate) + '-' + this.getDisplayableDate(this.toDate);
              }
            }
          } else { //Handles preprovided review date
            this.reviewCheckVal = "checked";
            this.auditCheckVal = "";
            this.dateReviewAuditType = "review";
            this.getcheckedVal("review");
            let reivewNgbDate = this.getNgbDateFromStringDate(curVal);
            this.calCurrSelectedDate = reivewNgbDate;
            this.dateTypeSelectionChanged('review');
            this.formattedDate = this.getDisplayableDate(reivewNgbDate);
          }
        }
      }
    }
  }

  /**
   * This method is called whenever a date is selected in the calendar.
   * @param inputdate 
   */
  onDateSelection(inputdate: NgbDate) {
    let date: NgbDate = new NgbDate(inputdate.year, inputdate.month, inputdate.day);
    this.fromDateErrorMsg = "";
    this.formattedDate = "";

    if (this.dateReviewAuditType == 'review') {
      this.toDate = null;
      this.fromDate = null;
      this.hoveredDate = null;
      this.formattedDate = this.getDisplayableDate(date);

    } else if (this.dateReviewAuditType == 'audit') {
      this.fromDate = date;

      let tempFromDate: Date = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      //Gets the date of the week starting when a date is selected within that week 
      tempFromDate = this.getBackDatedDateByDays(tempFromDate, tempFromDate.getDay());
      this.fromDate = new NgbDate(tempFromDate.getFullYear(), tempFromDate.getMonth() + 1, tempFromDate.getDate());

      let tempToDate: Date = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
      //Gets the date of the week ending when a date is selected within that week 
      tempToDate = this.getFutureDatedDateByDays(tempToDate, 6);
      let tempNgbToDate: NgbDate = new NgbDate(tempToDate.getFullYear(), tempToDate.getMonth() + 1, tempToDate.getDate());
      this.toDate = tempNgbToDate;

      this.checkFutureDate(tempNgbToDate);

      if (this.fromDate) {
        this.checkFromDateValidity(this.fromDate);
      }
      if (this.toDate) {
        this.checkFutureDate(this.toDate);
      }
      if (this.fromDate && this.toDate) {
        let isValid = this.daysDiffernceValid(this.fromDate, this.toDate);
        if (isValid) {
          this.formattedDate = this.getDisplayableDate(this.fromDate) + '-' + this.getDisplayableDate(this.toDate);
        }
      }
    }
  }

  /**
   * This method is called on click of radio buttons review/audit.
   * @param selectedVal 
   */
  dateTypeSelectionChanged(selectedVal: string) {

    this.dateReviewAuditType = selectedVal;
    this.fromDateErrorMsg = "";

    if (this.dateReviewAuditType == 'review') {
      this.reviewCheckVal = "checked";
      this.auditCheckVal = "";

      //Max date allowed for selection is till yesterday day
      let date: Date = this.getBackDatedDateByDays(new Date(), 1);
      let maxDate: NgbDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
      this.maxDateVal = maxDate;

      //Min date allowed for selection is till 20  previous business working days
      let reviewdate: Date = this.getPreviousSpecifiedWorkingsDaysBackDate();
      let minDate: NgbDate = new NgbDate(reviewdate.getFullYear(), reviewdate.getMonth() + 1, reviewdate.getDate());
      this.minDateVal = minDate;

    } else if (this.dateReviewAuditType == 'audit') {
      this.reviewCheckVal = "";
      this.auditCheckVal = "checked";
      let date: Date = new Date();
      let maxDate1: NgbDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
      this.maxDateVal = maxDate1;

      //Min date allowed for selection is last one year calendar days.
      let auditdate: Date = new Date();
      auditdate.setFullYear(auditdate.getFullYear() - 1);
      let minDate1: NgbDate = new NgbDate(auditdate.getFullYear(), auditdate.getMonth() + 1, auditdate.getDate());
      this.minDateVal = minDate1;
    }
  }

  datePcikerRef: any;

  getMinDateVal() {
    return this.minDateVal;
  }
  getMaxDateVal(datePicker: any) {
    if (datePicker) {
      this.datePcikerRef = datePicker;
    }
    return this.maxDateVal;
  }

  /**
   * Prepends zeros for month and date if they are single digit numbers
   * @param dateOrMonthVal 
   */
  prependZeroForMonthORDate(dateOrMonthVal: number): string {
    let dateOrMonthStr = dateOrMonthVal.toString();
    if (dateOrMonthStr.length < 2) {
      dateOrMonthStr = '0' + dateOrMonthStr;
    }
    return dateOrMonthStr;
  }

  /**
   * In case of audit date selection, the selection should in week intervals.
   * This method checks whether the difference between from date and to date is one week or not
   * @param fromDate 
   * @param toDate 
   */
  daysDiffernceValid(fromDate: NgbDate, toDate: NgbDate) {
    if (fromDate && toDate) {
      let fromDateJS: Date = new Date(fromDate.year, fromDate.month - 1, fromDate.day);
      let toDateJS: Date = new Date(toDate.year, toDate.month - 1, this.toDate.day);
      var timeDiff = Math.abs(toDateJS.getTime() - fromDateJS.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      if (diffDays != 6) {
        // this.fromDateErrorMsg = "Starting day must be Sunday and Ending Date must be Saturday";
        this.toDate = null;
        this.fromDate = null;
        this.hoveredDate = null;
        return false;
      } else {
        return true;
      }
    }
  }

  /**
   * In audit date selection, current week is not eligible for selection.
   * If user tries select current week date then the selected dates will be cleared.
   * @param fromDate 
   */
  checkFromDateValidity(fromDate: NgbDate) {

    let currentDate: Date = new Date();
    let selectedFromDate: Date = new Date(fromDate.year, fromDate.month - 1, fromDate.day);
    var timeDiff = Math.abs(currentDate.getTime() - selectedFromDate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    let currWeekDay = currentDate.getDay();
    if (diffDays <= currWeekDay) {
      // this.fromDateErrorMsg = "Starting day must be Sunday and Ending Date must be Saturday";
      this.toDate = null;
      this.fromDate = null;
      this.hoveredDate = null;
      return false;
    } else {
      return true;
    }
  }

  /**
   * Converts a string date(20180820) to bootstrap NgbDate.
   * @param strDate 
   */
  getNgbDateFromStringDate(strDate: string) {
    let year = Number(strDate.substring(0, 4));
    let month = Number(strDate.substring(4, 6));
    let date = Number(strDate.substring(6, 8));
    let ngbDate: NgbDate = new NgbDate(year, month, date);
    return ngbDate;
  }

  isHovered = (inputdate: NgbDate) => {
    let date: NgbDate = new NgbDate(inputdate.year, inputdate.month, inputdate.day);
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }
  isInside = (inputdate: NgbDate) => {
    let date: NgbDate = new NgbDate(inputdate.year, inputdate.month, inputdate.day);
    return date.after(this.fromDate) && date.before(this.toDate)
  };
  isRange = (inputdate: NgbDate) => {
    let date: NgbDate = new NgbDate(inputdate.year, inputdate.month, inputdate.day);
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date)
  };

  isMouseAction = (inputdate: NgbDate) => {
    if (inputdate) {
      let date: NgbDate = new NgbDate(inputdate.year, inputdate.month, inputdate.day);
      this.hoveredDate = date;
    } else {
      this.hoveredDate = null;
    }
  };

  isEligibleForDisable = (date: NgbDate) => {
    //if (this.isWeekend(date) || this.isPublicHoliday(date)) {
    if (this.isWeekend(date)) {
      return true;
    } else {
      false;
    }
  }

  isWeekend = (date: NgbDate) => {
    if (this.dateReviewAuditType == 'review') {
      return this.calendar.getWeekday(date) >= 6;
    }
  }

  // isPublicHoliday = (date: NgbDate) => {
  //   if (this.dateReviewAuditType == 'review') {
  //     if (date && date.month && date.day) {
  //       let result: boolean = this.holidayService.isHoliday("ca", date.month, date.day)
  //       return result;
  //     } else {
  //       return false;
  //     }
  //   }
  // }

  getcheckedVal(checkedVal: string) {
    if (checkedVal == this.dateReviewAuditType) {
      return 'checked';
    } else {
      return '';
    }
  }

  /**
   * Clears selected date properties if the user selects a future date.
   * @param ngbDate 
   */
  checkFutureDate(ngbDate: NgbDate) {
    let date: Date = new Date();
    let currDate: NgbDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
    if (ngbDate.after(currDate)) {
      this.toDate = null;
      this.fromDate = null;
      this.hoveredDate = null;
    }
  }

  /**
   * The selected date will be send to the parent component
   */
  populateDate() {
    this.selectedDateChange.emit(this.formattedDate);
  }

  /**
   * In review only the last 20 business working days should be enabled from todays date.
   */
  getPreviousSpecifiedWorkingsDaysBackDate(): Date {
    let previousdayDate: Date = this.getBackDatedDateByDays(new Date(), 1);
    let workingDaysCounter: number = 0;

    while (true) {
      let weekDay = previousdayDate.getDay();
      //let isDateDisabled = this.isPublicHoliday(new NgbDate(previousdayDate.getFullYear(), previousdayDate.getMonth() + 1, previousdayDate.getDate()))
      //if (weekDay != 0 && weekDay != 6 && !isDateDisabled) { //0 - sunday, 6 - saturday
      if (weekDay != 0 && weekDay != 6) {
        workingDaysCounter = workingDaysCounter + 1;
      }
      if (workingDaysCounter >= 20) {
        break;
      }
      previousdayDate = this.getBackDatedDateByDays(previousdayDate, 1);
    }
    return previousdayDate;
  }

  closeCalnedarPopup() {
    this.togglecalendar.emit("close");
  }

  /**
   * Sets the input date to specified number of past day date 
   */
  getBackDatedDateByDays(inputDate: Date, backDays: number) {

    let onedayOffset = backDays * (24 * 60 * 60 * 1000);
    inputDate.setTime(inputDate.getTime() - onedayOffset);
    return inputDate;
  }

  /**
   * Sets the input date to specified number of future day date 
   */
  getFutureDatedDateByDays(inputDate: Date, backDays: number) {

    let onedayOffset = backDays * (24 * 60 * 60 * 1000);
    inputDate.setTime(inputDate.getTime() + onedayOffset);
    return inputDate;
  }

  /**
   * This metod will return date with zero appended for month and date
   * @param inputDate 
   */
  getDisplayableDate(inputDate: NgbDate) {
    if (inputDate) {
      let monthVal = this.prependZeroForMonthORDate(inputDate.month);
      let dayVal = this.prependZeroForMonthORDate(inputDate.day);

      return inputDate.year+monthVal+dayVal;
    } else {
      return "";
    }
  }

  /**
   * Shows the calendar with date from process input date field
   */
  showSelectedDate() {
    if (this.datePcikerRef) {
      if (this.formattedDate) {
        let tempNgbDate = this.getNgbDateFromStringDate(this.formattedDate);
        if (tempNgbDate && tempNgbDate.year && tempNgbDate.month) {
          this.datePcikerRef.navigateTo({ year: tempNgbDate.year, month: tempNgbDate.month });
        }
      }
    }
  }
  ispublicholiday(){
    var hd = new Holidays.default('CA');
    let holidays = hd.getHolidays('2018');
    // Adjust for the weekends
    holidays.map((holiday) => {
      const date = new Date(holiday.date);
      if (date.getDay() === 0) {
        date.setDate(date.getDate() + 1);
        holiday.date = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} 00:00:00`;
        holiday.start.setDate(date.getDate());
        holiday.end.setDate(date.getDate() + 1);
        return holiday;
      }
      if (date.getDay() === 6) {
        date.setDate(date.getDate() + 2);
        holiday.date = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} 00:00:00`;
        holiday.start.setDate(date.getDate());
        holiday.end.setDate(date.getDate() + 1);
        return holiday;
      }
    });
  }
  

  }










